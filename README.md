hashie-rs
===
![A screenshot of the program's main window.](https://gitlab.com/Lynnesbian/hashie-rs/-/raw/main/res/screenshot.png)

## Description
Hashie is a GUI program for calculating hash digests of files. It runs on Linux, macOS, and Windows.

## Cloning
This repository makes use of [Git LFS](https://git-lfs.com/), so make sure you have it installed before cloning.

## Building
This program uses [`rfd`](https://github.com/PolyMeilex/rfd), which requires the GTK3 development headers to be 
installed if building for Linux (and BSDs). For information on how to install the headers, see
[the rfd documentation](https://docs.rs/rfd/latest/rfd/#gtk-backend).

## License
Licensed under the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0), with the following exceptions:
- The fonts under `res/` are licensed under the [SIL Open Font License 
 1.1](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web)
- The screenshot under `res/` is licensed under [Creative Commons 4.0 Attribution 
 Sharealike](https://creativecommons.org/licenses/by-sa/4.0/)
