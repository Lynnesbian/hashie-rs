#!/usr/bin/env bash
set -e

echo "Creating directories..."
mkdir -p res/dl

echo "Downloading latest IBM Plex release..."
curl -s "https://api.github.com/repos/IBM/Plex/releases/latest" \
| grep -P --only-matching "https://.*/v(\d+.\d+.\d+)/OpenType.zip" \
| xargs curl -Lo "res/dl/plex.zip"

echo "Extracting zip..."
unzip "res/dl/plex.zip" -d "res/dl"

echo "Cleaning up..."
mv "res/dl/OpenType/IBM-Plex-Mono/IBMPlexMono-Regular.otf" "res/ibm-plex-mono.otf"
mv "res/dl/OpenType/IBM-Plex-Sans/IBMPlexSans-Regular.otf" "res/ibm-plex-sans.otf"
rm -r "res/dl"

echo "Done."
