use std::borrow::Borrow;
use std::collections::HashMap;
use std::fs::{File, OpenOptions};
use std::path::Path;
use std::sync::mpsc::{Receiver, Sender};
use std::time::Duration;

use eframe::egui;
use eframe::egui::{
	Button, Color32, ComboBox, Direction, FontSelection, Layout, Stroke, TextEdit, TextStyle, TextureHandle, Vec2, Widget,
};
use egui_notify::Toasts;
use image::codecs::png::{CompressionType, FilterType, PngEncoder};
use image::{ColorType, ImageBuffer, ImageEncoder};
use libhashie::{CodeType, Algorithm};
use memmap2::MmapOptions;
use strum::IntoEnumIterator;
use tracing_subscriber::fmt::MakeWriter;

use crate::CODE_SIZE;

pub struct Hashie {
	pub file: String,
	pub algo: Algorithm,
	pub digest: String,
	pub toasts: Toasts,
	pub sender: Sender<Result<String, std::io::Error>>,
	pub receiver: Receiver<Result<String, std::io::Error>>,
	pub thread: Option<std::thread::JoinHandle<()>>,
	pub code: Option<TextureHandle>,
	pub code_type: CodeType,
	pub cache: HashMap<Algorithm, String>,
}

impl eframe::App for Hashie {
	#[allow(clippy::too_many_lines)]
	fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
		let mut file_changed = false;

		egui::CentralPanel::default().show(ctx, |ui| {
			// check for incoming hash
			if let Ok(digest) = self.receiver.try_recv() {
				// handle errors
				if let Err(e) = digest {
					self.toasts.error(e.to_string());
					self.digest = String::new();
				} else {
					self.digest = digest.unwrap();
				}
				self.thread = None;
				self.code = None;

				// update cache
				self.cache.insert(self.algo, self.digest.clone());

				if self.code_type != CodeType::None {
					let image = self.code_type.render(&self.digest, Some(CODE_SIZE));

					// convert image to egui texture
					if let Ok(image) = image {
						self.code = Some(image_to_egui_texture(ui.ctx(), &image));
					} else {
						self.toasts.error(image.unwrap_err().to_string());
					}
				}
			}

			// disable UI while calculating hash
			ui.set_enabled(self.thread.is_none());

			ui.horizontal(|ui| {
				egui::Frame::none()
					.rounding(egui::Rounding::same(2.0))
					.stroke(Stroke::new(1.0, Color32::from_gray(64)))
					.show(ui, |ui| {
						if TextEdit::singleline(&mut self.file)
							.desired_width(ui.available_width() - 100.0)
							.hint_text("Path to file")
							.ui(ui)
							.changed()
						{
							file_changed = true;
						};
					});

				if Button::new("Browse...").min_size(Vec2::new(82.0, 0.0)).ui(ui).clicked() {
					if let Some(path) = rfd::FileDialog::new().pick_file() {
						self.file = path.display().to_string();
						file_changed = true;
					}
				};
			});

			ui.add_space(10.0);

			let old_algo = self.algo;

			ui.horizontal(|ui| {
				ComboBox::from_id_source("algorithm")
					.selected_text(self.algo.to_string())
					.width(175.0)
					.show_ui(ui, |ui| {
						for algo in Algorithm::iter() {
							ui.selectable_value(&mut self.algo, algo, algo.to_string());
						}
					});

				ui.label(format!("{}, {} bit digest", &self.algo.name(), &self.algo.bits()));

				// only show spinner when background thread is processing
				if self.thread.is_some() {
					ui.spinner();
				}
			});

			ui.add_space(10.0);

			// if a new file or algorithm was selected, recalculate the hash
			if file_changed || old_algo != self.algo {
				// out with the old
				self.code = None;
				self.digest = String::new();

				// clone sender
				let tx = self.sender.clone();
				let file = self.file.clone();
				let algo = self.algo;

				// drop cache if file changed
				if file_changed {
					self.cache.clear();
				};

				// ensure the file can be opened
				if self.file.is_empty() {
					// do nothing
				} else if File::open(&self.file).is_err() {
					self.toasts.error("Failed to open file");
				} else if self.cache.contains_key(&self.algo) {
					// use cached value
					self.sender.send(Ok(self.cache[&self.algo].clone())).unwrap();
				} else {
					// delete old image
					self.code = None;

					// spawn worker thread
					self.thread = Some(std::thread::spawn(move || {
						// open file
						let file = File::open(file);
						if let Err(e) = file {
							tx.send(Err(e)).unwrap();
							return;
						}

						// memory map
						let mmap = unsafe { MmapOptions::new().map(&file.unwrap()) };
						if let Err(e) = mmap {
							tx.send(Err(e)).unwrap();
							return;
						}

						tx.send(Ok(algo.digest(&mmap.unwrap()[..]))).unwrap();
					}));
				}
			}

			ui.horizontal(|ui| {
				let digest_width = if self.code.is_some() {
					ui.available_width() - CODE_SIZE as f32 - 10.0
				} else {
					ui.available_width()
				};
				egui::Frame::none()
					.rounding(egui::Rounding::same(2.0))
					.stroke(Stroke::new(1.0, Color32::from_gray(128)))
					.show(ui, |ui| {
						ui.allocate_ui_with_layout(
							Vec2::new(digest_width, CODE_SIZE as f32),
							Layout::centered_and_justified(Direction::LeftToRight),
							|ui| {
								TextEdit::multiline(&mut (self.digest.borrow()))
									.hint_text("Digest will appear here")
									.desired_width(digest_width)
									.clip_text(false)
									.font(FontSelection::Style(TextStyle::Monospace))
									.ui(ui);
							},
						);
					});

				if let Some(code) = &self.code {
					egui::Frame::none()
						.stroke(Stroke::new(5.0, Color32::from_gray(255)))
						.show(ui, |ui| {
							ui.image(code);
						});
				}
			});

			ui.add_space(15.0);

			ui.add_enabled_ui(!self.digest.is_empty(), |ui| {
				ui.horizontal(|ui| {
					if ui.button("Copy").clicked() {
						ui.output_mut(|o| o.copied_text.clone_from(&self.digest));
						self.toasts.info("Copied!").set_duration(Some(Duration::from_secs(2)));
					};

					ui.label("Code type:");

					let old_code = self.code_type;

					ComboBox::from_id_source("code")
						.selected_text(self.code_type.to_string())
						.width(50.0)
						.show_ui(ui, |ui| {
							for code in CodeType::iter() {
								ui.selectable_value(&mut self.code_type, code, code.to_string());
							}
						});

					if old_code != self.code_type && self.code_type != CodeType::None {
						let image = self.code_type.render(&self.digest, Some(CODE_SIZE));

						// convert image to egui texture
						if let Ok(image) = image {
							self.code = Some(image_to_egui_texture(ui.ctx(), &image));
						} else {
							self.code = None;
							self.toasts.error(image.unwrap_err().to_string());
						}
					} else if self.code_type == CodeType::None {
						self.code = None;
					}

					ui.add_enabled_ui(self.code_type != CodeType::None, |ui| {
						if ui.button("Export Code").clicked() {
							if let Some(path) = rfd::FileDialog::new().set_file_name("code.png").save_file() {
								// regenerate code without scaling
								let image = self.code_type.render(&self.digest, None);

								if let Err(err) = image.as_deref() {
									self.toasts.error(err.to_string());
								}
								let image = image.unwrap();

								match export_code(&image, path.as_path()) {
									Err(e) => self.toasts.error(e),
									Ok(s) => self.toasts.info(s),
								};
							}
						}
					});
				})
			});

			self.toasts.show(ctx);
		});
	}
}

fn export_code(image: &ImageBuffer<image::Luma<u8>, Vec<u8>>, path: &Path) -> Result<String, String> {
	let file = OpenOptions::new()
		.create(true)
		.write(true)
		.truncate(true)
		.open(path)
		.map_err(|e| e.to_string())?;

	// write!
	let encoder = PngEncoder::new_with_quality(file.make_writer(), CompressionType::Best, FilterType::NoFilter);
	encoder
		.write_image(
			image.as_flat_samples().as_slice(),
			image.width(),
			image.height(),
			ColorType::L8.into(),
		)
		.map_err(|e| e.to_string())?;

	// open path in file manager
	if let Some(path) = path.parent() {
		open::that(path).unwrap_or(());
	}

	Ok(String::from("Image saved."))
}

fn image_to_egui_texture(ctx: &egui::Context, image: &ImageBuffer<image::Luma<u8>, Vec<u8>>) -> TextureHandle {
	let size = [image.width() as _, image.height() as _];
	let pixels = image.as_flat_samples();
	let egui_image = egui::ColorImage::from_gray(size, pixels.as_slice());
	ctx.load_texture("code", egui_image, egui::TextureOptions::default())
}
