// hide console window on Windows in release
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::collections::HashMap;
use std::sync::Arc;

use eframe::egui::{Style, Vec2, ViewportBuilder, Visuals};
use eframe::{egui, Theme};
use egui_notify::Toasts;
use libhashie::{Algorithm, CodeType};

use crate::ui::Hashie;

mod ui;

pub const CODE_SIZE: u32 = 128;

fn main() -> Result<(), eframe::Error> {
	// Log to stdout (if you run with `RUST_LOG=debug`).
	tracing_subscriber::fmt::init();

	let options = eframe::NativeOptions {
		viewport: ViewportBuilder::default()
			.with_inner_size(Vec2::new(380.0, 245.0))
			.with_min_inner_size(Vec2::new(340.0, 245.0)),
		default_theme: Theme::Light,
		..Default::default()
	};

	let res = eframe::run_native(
		"Hashie",
		options,
		Box::new(|cc| {
			// create mpsc channel
			let (sender, receiver) = std::sync::mpsc::channel::<_>();

			// load custom fonts
			let mut fonts = egui::FontDefinitions::empty();
			fonts.font_data.insert(
				String::from("IBM Plex Mono"),
				egui::FontData::from_static(include_bytes!("../res/ibm-plex-mono.otf")),
			);
			fonts.font_data.insert(
				String::from("IBM Plex Sans"),
				egui::FontData::from_static(include_bytes!("../res/ibm-plex-sans.otf")),
			);

			fonts
				.families
				.entry(egui::FontFamily::Proportional)
				.or_default()
				.insert(0, String::from("IBM Plex Sans"));
			fonts
				.families
				.entry(egui::FontFamily::Monospace)
				.or_default()
				.insert(0, String::from("IBM Plex Mono"));

			cc.egui_ctx.set_fonts(fonts);

			// initialise image loaders
			egui_extras::install_image_loaders(&cc.egui_ctx);

			// disable label text selection
			let mut style = Style {
				visuals: match cc.integration_info.system_theme {
					Some(Theme::Dark) => Visuals::dark(),
					_ => Visuals::light(),
				},
				..Default::default()
			};
			style.interaction.selectable_labels = false;
			cc.egui_ctx.set_style(Arc::new(style));

			// go!
			Box::new(Hashie {
				file: String::new(),
				algo: Algorithm::SHA256,
				digest: String::new(),
				toasts: Toasts::default(),
				sender,
				receiver,
				thread: None,
				code: None,
				code_type: CodeType::None,
				cache: HashMap::new(),
			})
		}),
	);

	match res {
		Ok(()) => Ok(()),
		Err(e) => {
			// show a message box detailing the error
			rfd::MessageDialog::new()
				.set_title("Error")
				.set_description(format!("hashie-rs failed to launch!\n{e}").as_str())
				.show();
			Err(e)
		}
	}
}
