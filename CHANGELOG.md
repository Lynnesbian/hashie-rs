# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Changed
- Use better image compression settings when exporting QR code PNG

## 0.2.0 - 2023-03-01
### Added
- [xxHash 3](https://github.com/Cyan4973/xxHash) support
- Label showing the name and digest bit size of the selected algorithm
- QR Code images can now be exported as PNG files

### Changed
- Increased the size of the QR code by reducing the quiet zone
- Use a cache to avoid recalculating hash digests for the same file

### Fixed
- Better error handling (missing read permissions, not a file, etc)

## 0.1.1 - 2023-02-28
### Fixed
- Don't omit leading zeroes in checksum output (oops!)

## 0.1.0 - 2023-02-25
Initial release.

### Added
- Support for various algorithms
	+ [BLAKE2](https://en.wikipedia.org/wiki/BLAKE_(hash_function)#BLAKE2)
	+ [MD5](https://en.wikipedia.org/wiki/MD5)
	+ [SHA-1](https://en.wikipedia.org/wiki/SHA-1)
	+ [SHA256](https://en.wikipedia.org/wiki/SHA-2)
	+ [SHA512](https://en.wikipedia.org/wiki/SHA-2)
	+ [SHA3-512](https://en.wikipedia.org/wiki/SHA-3)
- [QR Code](https://en.wikipedia.org/wiki/QR_code) generation
