#!/bin/bash

# SPDX-FileCopyrightText: 2021 Lynnesbian
# SPDX-License-Identifier: CC0-1.0

set -e
source "$HOME"/.cargo/env || true

_extra=""
_ver=""
if [ "$1" == "ci" ]; then
  # deny on warnings when running in CI
 _extra="-Dwarnings"
elif [ "$1" == "nightly" ]; then
	_ver="+nightly"
fi

# allow find to fail
find . -name '*.rs' -exec touch "{}" \; || true

cargo $_ver clippy --tests -- \
 -W clippy::nursery \
 -W clippy::perf \
 -W clippy::pedantic \
 -W clippy::complexity \
 -W clippy::style \
 -A clippy::cast_precision_loss \
 "$_extra"


# ALLOWS:

